require 'spec_helper'

describe StreemCommunicator::Sender do
  let(:sender){described_class}
  let(:socket_double){double('TCPSocket')}

  it{expect(sender).to respond_to(:send_message).with(2).argument}

  it "TCPSocket should receive write and gets once" do
    expect(socket_double).to receive(:write).once.with(kind_of(String))
    expect(socket_double).to receive(:gets).once.and_return("{\"status\":\"ok\",\"message\":\"subscribed to probes\"}")
    sender.send_message(socket_double, "message")
  end

  it do
    allow(socket_double).to receive(:write)
    allow(socket_double).to receive(:gets).and_return("{\"status\":\"ok\",\"message\":\"subscribed to probes\"}")
    expect(sender.send_message(socket_double, "{\"subscribe\": \"probes\"}")).to eq("subscribed to probes")
  end

  it do
    allow(socket_double).to receive(:write)
    allow(socket_double).to receive(:gets).and_return("{\"status\":\"error\",\"message\":\"unrecognized command\"}")
    expect{sender.send_message(socket_double, "{\"malformed message\": 1}")}.to raise_error(StreemCommunicator::Errors::CommandError, /unrecognized command/)
  end

  it do
    allow(socket_double).to receive(:write)
    allow(socket_double).to receive(:gets).and_return("{\"malformed\":\"message\",\"message\":\"Json out of specification\"}")
    expect{sender.send_message(socket_double, "{\"malformed message\": 1}")}.to raise_error(StreemCommunicator::Errors::MalformedResponseError, /{"malformed":"message","message":"Json out of specification\"}/)
  end

  it do
    allow(socket_double).to receive(:write)
    stub_const("StreemCommunicator::TIMEOUT", 1)
    allow(socket_double).to receive(:gets) do
      sleep(2)
    end
    expect{sender.send_message(socket_double, "{\"subscribe\": \"probes\"}")}.to raise_error(StreemCommunicator::Errors::TimeoutError)
  end

end
