require 'spec_helper'

describe StreemCommunicator::Commands do
  let(:commands){described_class}
  let(:message_1){Oj.load File.read(example_message(1))}

  it{expect(commands).to respond_to(:submit).with(1).argument}
  it{expect(commands).to respond_to(:subscribe).with(1).argument}
  it{expect(commands).to respond_to(:unsubscribe).with(1).argument}
  it{expect(commands).to respond_to(:register).with(1).argument}
  it{expect(commands).to respond_to(:unregister).with(1).argument}

  it{expect(commands.subscribe("message")).to eq("{\"subscribe\":\"message\"}\n") }
  it{expect(commands.unsubscribe("message")).to eq("{\"unsubscribe\":\"message\"}\n") }
  it{expect(commands.register("message")).to eq("{\"register\":\"message\"}\n") }
  it{expect(commands.unregister("message")).to eq("{\"unregister\":\"message\"}\n") }

  context "message validation is stubbed" do
    before(:each) do
      commands.stub_chain(:validator, :validate!).and_return(nil)
    end
    it{expect(commands.submit("message")).to eq("{\"submit\":\"message\"}\n") }
    it{expect(commands.submit({"value" => 1, "name" => "str"})).to eq("{\"submit\":{\"value\":1,\"name\":\"str\"}}\n") }
  end

  context "validate message before submit" do
    it{expect{commands.submit("message")}.to raise_error(StreemCommunicator::Errors::VersionNotSpecifiedError, /supported version 1,2,3/)}
    it{expect{commands.submit({"value" => 1, "name" => "str", "v" => "2"})}.to raise_error(JSON::Schema::ValidationError)}
  end

  it{expect(commands.new_line_at_end("msg")).to eq("msg\n")}
end
