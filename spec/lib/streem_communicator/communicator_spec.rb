require 'spec_helper'

describe StreemCommunicator::Communicator do
  let(:communicator){described_class.new("localhost", 1000)}
  let(:socket_mock){double('TCPSocket')}
  let(:message_1){Oj.load File.read(example_message(1))}

  before(:each) do
    allow(TCPSocket).to receive(:new).and_return(socket_mock)
    allow(StreemCommunicator::Sender).to receive(:send_message).and_return(true)
  end

  it{expect(communicator).to respond_to(:submit).with(1).argument}
  it{expect(communicator).to respond_to(:subscribe).with(1).argument}
  it{expect(communicator).to respond_to(:unsubscribe).with(1).argument}
  it{expect(communicator).to respond_to(:register).with(1).argument}
  it{expect(communicator).to respond_to(:unregister).with(1).argument}

  it{expect(communicator).to respond_to(:listen).with(0).arguments}
  it{expect(communicator).to respond_to(:receive).with(0).arguments}

  it{expect(communicator).to respond_to(:unsubscribe_all).with(0).arguments}
  it{expect(communicator).to respond_to(:unregister_all).with(0).arguments}

  it{expect(communicator).to respond_to(:subscribe_streems).with(0).arguments}
  it{expect(communicator).to respond_to(:register_streems).with(0).arguments}

  context ".subscribe(\"probes\")" do
    it "should send subscribe message" do
      expect(StreemCommunicator::Sender).to receive(:send_message).with(socket_mock, "{\"subscribe\":\"probes\"}\n").once
      communicator.subscribe("probes")
    end

    it "should add \"probes\" to subscrib_streems list" do
      communicator.subscribe("probes")
      expect(communicator.subscribe_streems).to include("probes")
    end
  end

  context ".unsubscribe(\"probes\")" do

    it "should send unsubscribe message" do
      communicator.subscribe("probes")
      expect(StreemCommunicator::Sender).to receive(:send_message).with(socket_mock, "{\"unsubscribe\":\"probes\"}\n").once
      communicator.unsubscribe("probes")
    end

    it "should remove probes from subscribe_streem list" do
      communicator.subscribe("probes")
      communicator.unsubscribe("probes")
      expect(communicator.subscribe_streems).not_to include("probes")
    end

    it "probes in not subscribed should raise MissingStreemError" do
      communicator.subscribe("probes2")
      expect{communicator.unsubscribe("probes")}.to \
                    raise_error(StreemCommunicator::Errors::MissingStreemError)
    end

    it "probe_streems has empty list should raise EmptyStreemListError" do
      expect{communicator.unsubscribe("probes")}.to \
                    raise_error(StreemCommunicator::Errors::EmptyStreemListError)
    end
  end

  context ".unregister(\"probes\")" do

    it "should send unregister message" do
      communicator.register("probes")
      expect(StreemCommunicator::Sender).to receive(:send_message).with(socket_mock, "{\"unregister\":\"probes\"}\n").once
      communicator.unregister("probes")
    end

    it "should remove probes from register_streem list" do
      communicator.register("probes")
      communicator.unregister("probes")
      expect(communicator.register_streems).not_to include("probes")
    end

    it "probes in not registerd should raise MissingStreemError" do
      communicator.register("probes2")
      expect{communicator.unregister("probes")}.to \
                    raise_error(StreemCommunicator::Errors::MissingStreemError)
    end

    it "probe_streems has empty list should raise EmptyStreemListError" do
      expect{communicator.unregister("probes")}.to \
                    raise_error(StreemCommunicator::Errors::EmptyStreemListError)
    end
  end

  context ".register(\"probes\")" do
    it "should send receive message" do
      expect(StreemCommunicator::Sender).to receive(:send_message).with(socket_mock, "{\"register\":\"probes\"}\n").once
      communicator.register("probes")
    end

    it "should add \"probes\" to register_streems list" do
      communicator.register("probes")
      expect(communicator.register_streems).to include("probes")
    end
  end

  context ".unregister_all" do
    it "for two registred streems should receive two times unregister message" do
      communicator.register("probes")
      communicator.register("probes2")
      expect(StreemCommunicator::Sender).to receive(:send_message).twice
      communicator.unregister_all
    end

    it "without any registred streems should raise EmptyStreemListError" do
      expect{communicator.unregister_all}.to\
                    raise_error(StreemCommunicator::Errors::EmptyStreemListError)
    end

    it "for two registred streems should registred_streems == []" do
      communicator.register("probes")
      communicator.register("probes2")
      communicator.unregister_all
      expect(communicator.register_streems).to eq([])
    end
  end

  context ".unsubscribe_all" do
    it "for two registred streems should receive two times unsubscribe message" do
      communicator.subscribe("probes")
      communicator.subscribe("probes2")
      expect(StreemCommunicator::Sender).to receive(:send_message).twice
      communicator.unsubscribe_all
    end

    it "without any registred streems should raise EmptyStreemListError" do
      expect{communicator.unsubscribe_all}.to\
                    raise_error(StreemCommunicator::Errors::EmptyStreemListError)
    end

    it "for two registred streems should registred_streems == []" do
      communicator.subscribe("probes")
      communicator.subscribe("probes2")
      communicator.unsubscribe_all
      expect(communicator.subscribe_streems).to eq([])
    end
  end

  context ".submit" do
    it "when there is no streems register expect to raise EmptyStreemListError" do
      expect{communicator.submit({"Test" => "message"})}.to\
                    raise_error(StreemCommunicator::Errors::EmptyStreemListError)
    end

    it "submit message when there is any streem register" do
      communicator.register("probes")
      expect( StreemCommunicator::Sender ).to receive(:send_message).once
      communicator.submit(message_1)
    end
  end

  it "when there is no streems subscribed listen() expect to raise EmptyStreemListError" do
    expect{communicator.listen()}.to\
                  raise_error(StreemCommunicator::Errors::EmptyStreemListError)
  end

  it "when there is no streems subscribed receive() expect to raise EmptyStreemListError" do
    expect{communicator.receive()}.to\
                  raise_error(StreemCommunicator::Errors::EmptyStreemListError)
  end
end

