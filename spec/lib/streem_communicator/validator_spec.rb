require 'spec_helper'
require 'oj'

describe StreemCommunicator::Validator do
  let(:validator){described_class.new}
  let(:message_1){Oj.load File.read(example_message(1))}
  let(:message_2){Oj.load File.read(example_message(2))}
  let(:message_3){Oj.load File.read(example_message(3))}
  let(:message_4){Oj.load File.read(example_message(4))}
  let(:invalid){Oj.load File.read(File.join(example_path, "message_v2_invalid.json"))}
  let(:missing_version){Oj.load File.read(File.join(example_path, "message_missing_version.json"))}

  it{expect(validator).to respond_to(:validate).with(1).argument}
  it{expect(validator).to respond_to(:validate!).with(1).argument}

  it{expect(validator).to respond_to(:schema_chooser).with(1).argument}

  it{expect(validator).to respond_to(:message_schema_v1).with(0).arguments}
  it{expect(validator).to respond_to(:message_schema_v2).with(0).arguments}
  it{expect(validator).to respond_to(:message_schema_v3).with(0).arguments}

  it{expect(validator.message_schema_v1).to be_a(String) }
  it{expect(validator.message_schema_v2).to be_a(String) }
  it{expect(validator.message_schema_v3).to be_a(String) }

  it{expect{validator.schema_chooser(4)}.to raise_error(StreemCommunicator::Errors::UnsupportedVersionError, /supported version 1,2,3/)}

  it{expect(validator.validate(message_1)).to be_true}
  it{expect(validator.validate(message_2)).to be_true}
  it{expect(validator.validate(message_3)).to be_true}
  it{expect(validator.validate(message_4)).to be_false}
  it{expect(validator.validate(invalid)).to be_false}
  it{expect(validator.validate(missing_version)).to be_false}

  it{expect(validator.validate!(message_1)).to be_true}
  it{expect(validator.validate!(message_2)).to be_true}
  it{expect(validator.validate!(message_3)).to be_true}
  it{expect{validator.validate!(message_4)}.to raise_error(StreemCommunicator::Errors::UnsupportedVersionError, /supported version 1,2,3/)}
  it{expect{validator.validate!(missing_version)}.to raise_error(StreemCommunicator::Errors::VersionNotSpecifiedError, /supported version 1,2,3/)}
  it{expect{validator.validate!(invalid)}.to raise_error(JSON::Schema::ValidationError)}
end

