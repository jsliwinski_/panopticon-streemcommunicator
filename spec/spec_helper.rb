require './lib/streem_communicator'
RSpec.configure do |config|
  config.run_all_when_everything_filtered = true

  config.order = 'random'

  def example_path
    File.join(File.dirname(__FILE__), 'examples')
  end

  def example_message(version)
    File.join(example_path, "message_v#{version}.json")
  end

end
