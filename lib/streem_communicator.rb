require "streem_communicator/version"
require "streem_communicator/commands"
require "streem_communicator/communicator"
require "streem_communicator/sender"
require "streem_communicator/errors"
require "streem_communicator/validator"
require 'timeout'
require 'oj'

module StreemCommunicator
  TIMEOUT = 5
end
