module StreemCommunicator
  module Errors
    class CommandError < StandardError; end
    class TimeoutError < StandardError; end
    class MalformedResponseError < StandardError; end
    class MissingStreemError < StandardError; end
    class EmptyStreemListError < StandardError; end
    class UnsupportedVersionError < StandardError; end
    class VersionNotSpecifiedError < StandardError; end
  end
end
