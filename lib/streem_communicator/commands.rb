module StreemCommunicator
  module Commands

    class << self


      [:submit, :subscribe, :unsubscribe, :register, :unregister].each do |name|
        define_method(name) do |message|
          if(name == :submit)
            validator.validate!(message)
          end
          new_line_at_end(Oj.dump({"#{name}" => message}))
       end
      end

      def new_line_at_end(message)
        "#{message}\n"
      end

      def validator
        @validator ||= Validator.new
      end

    end

  end
end
