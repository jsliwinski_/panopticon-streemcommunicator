module StreemCommunicator
  module Sender
    class << self
      def send_message(socket, message)
        socket.write(message)
        response = {}
        raw_response = ""
        Timeout.timeout(StreemCommunicator::TIMEOUT, Errors::TimeoutError) do
          raw_response = socket.gets
          response = Oj.load(raw_response)
        end
        if(response.nil? || !response.is_a?(Hash) || !response.has_key?("status"))
          raise Errors::MalformedResponseError.new(raw_response)
        end
        if(response["status"] != "ok")
          raise Errors::CommandError.new(response["message"])
        end
        response["message"]
      end
    end
  end
end
