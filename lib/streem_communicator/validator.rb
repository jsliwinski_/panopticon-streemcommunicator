require 'json-schema'

module StreemCommunicator
  class Validator
    attr_reader :message_schema_v1, :message_schema_v2, :message_schema_v3

    SUPPORTED_MESSAGE_VERSIONS = %w(1 2 3).map(&:to_i)

    SUPPORTED_MESSAGE_VERSIONS.each do |version|
      define_method(:"message_schema_v#{version}") do
        variable_name = :"@message_schema_v#{version}"
        if !instance_variable_defined?(variable_name)
          file = File.read(schema_filename(version))
          instance_variable_set(variable_name, file)
        end
          return instance_variable_get(variable_name)
      end
    end

    def validate(message)
      validate!(message)
      return true
    rescue
      return false
    end

    def validate!(message)
      if !message.respond_to?(:has_key?) || !message.has_key?("v")
        raise Errors::VersionNotSpecifiedError.new("Message version not specified, supported version #{SUPPORTED_MESSAGE_VERSIONS.join(",")}")
      end
      version_str = message["v"]
      version_int = version_str.to_i
      unless SUPPORTED_MESSAGE_VERSIONS.include?(version_int)
        raise Errors::UnsupportedVersionError.new("Unsupported schema version: #{version_str}, supported version #{SUPPORTED_MESSAGE_VERSIONS.join(",")}")
      end
      schema = schema_chooser(version_int)
      JSON::Validator.validate!(schema, message)
    end

    def schema_chooser(version)
      if SUPPORTED_MESSAGE_VERSIONS & [version] == []
        raise Errors::UnsupportedVersionError.new("Unsupported schema version: #{version}, supported version #{SUPPORTED_MESSAGE_VERSIONS.join(",")}")
      end
      method_name = :"message_schema_v#{version}"
      send(method_name)
    end

    private
    def schema_filepath
      File.join(File.dirname(__FILE__), "schemas")
    end

    def schema_filename(version)
      File.join(schema_filepath, "message-schema-v#{version}.json")
    end

  end
end
