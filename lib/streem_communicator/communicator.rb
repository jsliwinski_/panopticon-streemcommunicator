module StreemCommunicator
  class Communicator

    attr_reader :port, :host, :subscribe_streems, :register_streems

    def initialize(host, port)
      @port = port
      @host = host
      @socket = TCPSocket.new(@host,@port)
      @subscribe_streems = []
      @register_streems = []
      @sender_module = StreemCommunicator::Sender
      @command_module = StreemCommunicator::Commands
    end

    def listen
      if(@subscribe_streems == [])
        raise Errors::EmptyStreemListError.new("Empty subscribe streem list")
      end
      Enumerator.new do |yielder|
        while line = @socket.gets
          yielder << Oj.load(line)
        end
      end.lazy
    end

    def receive
      listen.peek
    end

    def submit(message)
      if(@register_streems == [])
        raise Errors::EmptyStreemListError.new("Empy registred streem list")
      end
      response = @sender_module.send_message(@socket,
                                             @command_module.submit(message))
      return response
    end

    [:unsubscribe, :unregister].each do |name|
      define_method(name) do |streem_name|
        list_name = name[2..-1]
        streem_array = instance_variable_get(:"@#{list_name}_streems")
        if streem_array == []
          raise Errors::EmptyStreemListError.new("Empty list #{list_name}")
        end
        if !streem_array.include?(streem_name)
          raise Errors::MissingStreemError.new("Missing #{streem_name} on #{list_name} list")
        end
        response = @sender_module.send_message(@socket,
                                     @command_module.send(name, streem_name))
        streem_array.delete(streem_name)
        return response
      end
    end

    [:register, :subscribe].each do |name|
      define_method(name) do |streem_name|
        streem_array = instance_variable_get(:"@#{name}_streems")
        response = @sender_module.send_message(@socket,
                                     @command_module.send(name, streem_name))
        streem_array << streem_name
        return response
      end
    end

    [:unsubscribe_all, :unregister_all].each do |name|
      define_method(name) do
        list_name = name.to_s.gsub("_all", "")[2..-1]
        streem_array = instance_variable_get(:"@#{list_name}_streems").dup
        if streem_array == []
          raise Errors::EmptyStreemListError.new("Empty list #{list_name}")
        end
        method_name = "un" + list_name
        streem_array.each do |streem|
          self.send(method_name, streem)
        end
      end
    end
  end
end
