# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'streem_communicator/version'

Gem::Specification.new do |spec|
  spec.name          = "streem_communicator"
  spec.version       = StreemCommunicator::VERSION
  spec.authors       = ["Jakub Sliwinski"]
  spec.email         = ["sliwinski.jakub@gmail.com"]
  spec.summary       = %q{Connection wrapper for Panopticon streem}
  spec.description   = %q{Simple connection wrapper to Panopticon streem database}
  spec.homepage      = "http://panopticon.cf"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'oj', '~> 2.7', '>= 2.7.1'
  spec.add_dependency 'json-schema', '~> 2.2.2'

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake", "~> 0"
  spec.add_development_dependency "rspec", "~> 2.14"
  spec.add_development_dependency "guard", "~> 0"
  spec.add_development_dependency "guard-rspec", "~> 0"
  spec.add_development_dependency "pry", "~> 0"
  spec.add_development_dependency "pry-debugger", "~> 0"
end
